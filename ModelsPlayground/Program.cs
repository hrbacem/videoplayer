﻿using Microsoft.Win32;
using NWord2Vec;
using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word2Vec.Net;

namespace ModelsPlayground
{

    class Program
    {
        static List<String> PreparedModules;
        static void setModules()
        {
            PreparedModules = new List<string>();
            PreparedModules.Add(@"C:\Users\klist\source\repos\VideoPlayer\data\models\basicCZ.txt");
            PreparedModules.Add(@"C:\Users\klist\source\repos\VideoPlayer\data\models\enmodel.txt");
            PreparedModules.Add(@"C:\Users\klist\source\repos\VideoPlayer\data\models\longczmodel.txt");
            PreparedModules.Add(@"C:\Users\klist\source\repos\VideoPlayer\data\models\southParkModelEn.txt");
            PreparedModules.Add(@"C:\Users\klist\source\repos\VideoPlayer\data\models\wikiModelEN.txt");

        }

        static List<String> PreparedTraining;
        static void setTraining()
        {
            PreparedTraining = new List<string>();
            PreparedTraining.Add(@"C: \Users\klist\source\repos\VideoPlayer\data\learning\All-seasons.csvout.txt");
            PreparedTraining.Add(@"C: \Users\klist\source\repos\VideoPlayer\data\learning\trenovacitext.txt");
        }

        static string file;

        static string help = null;
        static string helpInfo()
        {
            if (help != null)
            {
                return help;
            }
            StringBuilder sb = new StringBuilder("help -> Print information.");
            sb.AppendLine("load -> Load given file with model.");
            sb.AppendLine("model -> Print name of file.");
            sb.AppendLine("train -> Train model on given file and use it as model.");
            sb.AppendLine("words -> Print words from model.");
            sb.AppendLine("nearest -> Print TOP 10 nearest word.");
            sb.AppendLine("distance -> Return distance of two words.");
            help = sb.ToString();
            return help;
        }

        static void trainModel()
        {
            Console.WriteLine("filename:");
            string trainfile;
            Console.WriteLine("Text to use:");
            for (int k = 0; k < PreparedTraining.Count; k++)
            {
                Console.WriteLine(k + ": " + PreparedTraining[k]);
            }
            Console.WriteLine("Or just type path....");
            string choosen = Console.ReadLine();



            if (int.TryParse(choosen, out int i))
            {
                if (i < 0 || PreparedTraining.Count <= i)
                {
                    Console.WriteLine("Out of text.");
                    return;
                }
                trainfile = PreparedTraining[i];
            }
            else
            {
                trainfile = choosen;
            }

            string outputFileName = trainfile + "out.txt";
            file = outputFileName;
            var word2Vec = Word2VecBuilder.Create()
                .WithTrainFile(trainfile)// Use text data to train the model;
                .WithOutputFile(outputFileName)//Use to save the resulting word vectors / word clusters
                .Build();

            word2Vec.TrainModel();

        }

       
        static Model model;

        static void Main(string[] args)
        {

            model = null;
            file = @"C:\Users\klist\source\repos\VideoPlayer\data\models\enmodel.txt";
            model = Model.Load(file);
            setModules();
            setTraining();
            Console.WriteLine("Default model: " + file);

            while (true)
            {
                Console.WriteLine();
                string comm = Console.ReadLine();
                Console.WriteLine(">");
                switch (comm)
                {
                   case "load":
                        {

                            Console.WriteLine("Models to use:");
                            for (int k = 0; k < PreparedModules.Count; k++)
                            {
                                Console.WriteLine(k + ": " + PreparedModules[k]);
                            }
                            Console.WriteLine("Or just type path....");
                            string choosen = Console.ReadLine();



                            if (int.TryParse(choosen, out int i))
                            {
                                if (i < 0 || PreparedModules.Count <= i)
                                {
                                    Console.WriteLine("Out of models.");
                                    break;
                                }
                                file = PreparedModules[i];
                            }
                            else
                            {
                                file = choosen;
                            }

                            try
                            {
                                model = Model.Load(file);
                                Console.WriteLine("Model loaded.");
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Invalid file!");
                            }



                        }
                        break;

                    case "model":
                        {
                            Console.WriteLine(file);
                        }
                        break;

                    case "words":
                        {
                            if (model == null)
                            {
                                Console.WriteLine("No model.");
                                break;
                            }
                            foreach (var item in model.Vectors)
                            {
                                Console.WriteLine(item.Word);
                            }
                        }
                        break;

                    case "nearest":
                        {
                            Console.WriteLine("Word: ");
                            try
                            {
                                string word = Console.ReadLine();
                                foreach (var item in model.Nearest(word).Take(10))
                                {
                                    Console.WriteLine(item.Word);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                        }
                        break;

                    case "distance":
                        {
                            Console.WriteLine("Word 1 :");
                            string word1 = Console.ReadLine();
                            Console.WriteLine("Word 2 :");
                            string word2 = Console.ReadLine();
                            try
                            {
                                Console.WriteLine(model.Distance(word1, word2));
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Cannot be done.");
                            }
                        }
                        break;

                    case "train":
                        {
                            trainModel();
                            Console.WriteLine("Load model: " + file);
                            model = Model.Load(file);
                            Console.WriteLine("Model loaded.");
                        }
                        break;

                    case "help":
                    default:
                        {
                            Console.WriteLine("Commands:");
                            Console.WriteLine(helpInfo());
                        }
                        break;
                }

            }
        }
    }
}
