﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SearchEngines
{
    class AudioToScript : IAudioToText
    {
        public bool tryAudioToTextConvert(string input, out string output, Language language)
        {
            output = input.Replace(".wav", ".srt");
            if (File.Exists(output)) return true;

            return AudioToTextConvert(input, out output, language);
        }

        public bool AudioToTextConvert(string input, out string output, Language language)
        {
            output = input.Replace(".wav", ".srt");
            
            try
            {
                string lang = toString(language);
                string strCmdText = "autosub " + "\""+input + "\"" + " -S " + lang + " -D " + lang;
                var process = System.Diagnostics.Process.Start("CMD.exe", "/C " + strCmdText);
                process.WaitForExit();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private static string toString(Language language)
        {
            switch (language)
            {
                case Language.czech:
                    return "cs";

                case Language.english:
                default:
                    return "en";
            }
        }

    }
}
