﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word2vec;
using NWord2Vec;


namespace SearchEngines
{
    class FullMatchSearchInBlock : ISearchInBlock
    {
        public bool SearchInBlock(string input, IQuery query, out string match)
        {
            if (input.ToUpper().Contains(query.Query.ToUpper()))
            {
                match = query.Query;
                return true;
            }
            match = "";
            return false;
        }
    }

    class PartialFullMatchSearchInBlock : ISearchInBlock
    {
        public bool SearchInBlock(string input, IQuery query, out string match)
        {
            char[] delimiterChars = { ' ', ',', ';', '.', ':', '\t' };
            string[] words = query.Query.ToUpper().Split(delimiterChars);
            foreach (var item in words)
            {
                if (item == "") continue;
                if (input.ToUpper().Contains(item.ToUpper()))
                {
                    match = item;
                    return true;
                }

            }
            match = "";
            return false;
        }
    }

    class RegexpSearchInBlock : ISearchInBlock
    {
        public bool SearchInBlock(string input, IQuery query, out string match)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(input.ToUpper(), query.Query.ToUpper()))
            {
                var matches = System.Text.RegularExpressions.Regex.Match(input, query.Query);
                match = matches.Value;
                return true;
            }
            match = "";
            return false;
        }
    }

    class ExperimentalMatch : ISearchInBlock
    {
        int lengthQuery;

        private double DistanceQ;

        public enum OPTIONS
        {
            Hamming,
            Levenshtein,
            DamerauLevenshtein
        }

        OPTIONS algorithmChoice;

        public ExperimentalMatch(OPTIONS alg)
        {
            algorithmChoice = alg;
        }

        public bool SearchInBlock(string input, IQuery query, out string match)
        {
            string word = query.Query.ToUpper();
            int lengthInput = input.Length;
            lengthQuery = query.Query.Length;

            DistanceQ = ((double)word.Length) * 0.2;
            if (DistanceQ < 1) DistanceQ = 1;
            // Walk throught all subwords
            for (int i = 0; i < lengthInput - lengthQuery; i++)
            {
                string subword = input.Substring(i, lengthQuery).ToUpper();
                switch (algorithmChoice)
                {
                    case OPTIONS.Hamming:
                        {
                            int distance = GetHammingDistance(word, subword);
                            if (distance <= DistanceQ)
                            {
                                match = subword;
                                return true;
                            }
                        }
                        break;
                    case OPTIONS.Levenshtein:
                        {
                            int distance = LevenshteinDistance(word, subword);
                            if (distance <= DistanceQ)
                            {
                                match = subword;
                                return true;
                            }
                        }
                        break;
                    case OPTIONS.DamerauLevenshtein:
                        {
                            int distance = GetDamerauLevenshteinDistance(word, subword);
                            if (distance <= DistanceQ)
                            {
                                match = subword;
                                return true;
                            }
                        }
                        break;
                }
            }
            match = "";
            return false;
        }

        public static int GetHammingDistance(string s, string t)
        {
            if (s.Length != t.Length)
            {
                throw new Exception("Strings must be equal length");
            }

            int distance =
                s.ToCharArray()
                .Zip(t.ToCharArray(), (c1, c2) => new { c1, c2 })
                .Count(m => m.c1 != m.c2);

            return distance;
        }

        public static int LevenshteinDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        public static int GetDamerauLevenshteinDistance(string s, string t)
        {
            var bounds = new { Height = s.Length + 1, Width = t.Length + 1 };

            int[,] matrix = new int[bounds.Height, bounds.Width];

            for (int height = 0; height < bounds.Height; height++) { matrix[height, 0] = height; };
            for (int width = 0; width < bounds.Width; width++) { matrix[0, width] = width; };

            for (int height = 1; height < bounds.Height; height++)
            {
                for (int width = 1; width < bounds.Width; width++)
                {
                    int cost = (s[height - 1] == t[width - 1]) ? 0 : 1;
                    int insertion = matrix[height, width - 1] + 1;
                    int deletion = matrix[height - 1, width] + 1;
                    int substitution = matrix[height - 1, width - 1] + cost;

                    int distance = Math.Min(insertion, Math.Min(deletion, substitution));

                    if (height > 1 && width > 1 && s[height - 1] == t[width - 2] && s[height - 2] == t[width - 1])
                    {
                        distance = Math.Min(distance, matrix[height - 2, width - 2] + cost);
                    }

                    matrix[height, width] = distance;
                }
            }

            return matrix[bounds.Height - 1, bounds.Width - 1];
        }
    }

    static class W2VModels
    {
        private static Model EN = null;
        private static string enModel = null;

        private static Model CZ = null;
        private static string czModel = null;

        public static Model getModel(Language lang)
        {
            switch (lang)
            {
                case Language.english:
                    {
                        if (enModel != MODELS.EnglishModel)
                        {
                            EN = Model.Load(MODELS.EnglishModel);
                            enModel = MODELS.EnglishModel;
                        }

                        return EN;
                    }
                    break;

                case Language.czech:
                    {
                        if (czModel != MODELS.CzechModel)
                        {
                            CZ = Model.Load(MODELS.CzechModel);
                            czModel = MODELS.CzechModel;
                        }
                        return CZ;
                    }
                    break;

                default:
                    return null;
            }

        }
    }

    class Word2VecSearchInBlock : ISearchInBlock
    {
        public Word2VecSearchInBlock()
        {

        }

        private bool search(string[] wordsQuery, string[] wordsInput, out string match)
        {
            match = "";
            foreach (var qe in wordsQuery)
            {

                double min = MODELS.MaximalDistance;
                foreach (var inp in wordsInput)
                {
                    try
                    {
                        var val = model.Distance(qe, inp);

                        if (val < min)
                        {
                            match = inp;
                            return true;
                        }
                    }
                    catch (Exception)
                    {
                        //throw new Exception(nearest + " "+ query.Language + " "+qe + " "+ inp);
                    }
                }

            }
            return false;
        }
        private Model model;
        public bool SearchInBlock(string input, IQuery query, out string match)
        {
            model = W2VModels.getModel(query.Language);
            if (model == null)
            {
                match = "";
                return false;
            }

            char[] delimiterChars = { ' ', ',', ';', '.', ':', '\t' };
            string[] wordsQuery = query.Query.Split(delimiterChars);
            string[] wordsInput = input.Split(delimiterChars);

            var regular = search(wordsQuery, wordsInput, out match);
            if (regular)
            {
                return true;
            }

            wordsQuery = wordsQuery.ToList().ConvertAll(s => s.ToUpper()).ToArray();
            wordsInput = wordsInput.ToList().ConvertAll(s => s.ToUpper()).ToArray();
            var upper = search(wordsQuery, wordsInput, out match);
            if (upper)
            {
                return true;
            }

            return false;
        }
    }


}
