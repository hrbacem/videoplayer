﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SearchEngines
{
    public class DefaultSearchEngine : ISearcher
    {
        public IResults Search(string file, IQuery query)
        {

            if (query.NewTranslateNeeded)
                return processMandatoryScript(file, query);
            else
                return processNotMandatoryScript(file, query);
        }

        private IResults processMandatoryScript(string file, IQuery query)
        {
            var res = new Results(file);

            VideoToAudioConvertor cv = new VideoToAudioConvertor();
            if (!cv.VideoToAudioConvert(file, out string outputWav))
            {
                res.failOnVideo = true;
                res.ErrorFileName = file + " -> " + outputWav;
                return res;
            }


            res.AudioSource = outputWav;
            string outSRT;
            AudioToScript aus = new AudioToScript();
            if (!aus.AudioToTextConvert(outputWav, out string outputSRT, query.Language))
            {
                res.failOnAudio = true;
                res.ErrorFileName = outputWav + " -> " + outputSRT;
                return res;
            }
            res.SubtitleSource = outputSRT;
            outSRT = outputSRT;



            SearchInSubtitlesDefault ssd = new SearchInSubtitlesDefault();
            if (!ssd.SearchInSubtitles(outSRT, query, res, out IResults result))
            {
                res.failOnSrt = true;
                res.ErrorFileName = " SEARCH-> " + outSRT;
                return res;
            }

            return result;
        }

        private IResults processNotMandatoryScript(string file, IQuery query)
        {
            var res = new Results(file);
            string outSRT;

            string extension = Path.GetExtension(file);

            //Exist WAV - USE  IT AS AUDIO
            string potentialOutputAudio = file.Replace(extension, ".wav");
            if (File.Exists(potentialOutputAudio))
            {
                res.AudioSource = potentialOutputAudio;
            }

            //Exist SRT - USE  IT AS Subtitle
            string potentialOutputSrt = file.Replace(extension, ".srt");
            if (File.Exists(potentialOutputSrt))
            {
                outSRT = potentialOutputSrt;
            }
            else
            {
                VideoToAudioConvertor cv = new VideoToAudioConvertor();
                if (!cv.VideoToAudioConvert(file, out string outputWav))
                {
                    res.failOnVideo = true;
                    res.ErrorFileName = file + " -> " + outputWav;
                    return res;
                }


                res.AudioSource = outputWav;

                AudioToScript aus = new AudioToScript();
                if (!aus.tryAudioToTextConvert(outputWav, out string outputSRT, query.Language))
                {
                    res.failOnAudio = true;
                    res.ErrorFileName = outputWav + " -> " + outputSRT;
                    return res;
                }
                res.SubtitleSource = outputSRT;
                outSRT = outputSRT;
            }


            SearchInSubtitlesDefault ssd = new SearchInSubtitlesDefault();
            if (!ssd.SearchInSubtitles(outSRT, query, res, out IResults result))
            {
                res.failOnSrt = true;
                res.ErrorFileName = " SEARCH-> " + outSRT;
                return res;
            }
            return result;


        }

    }
}
