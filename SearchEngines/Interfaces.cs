﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngines
{

    public interface ISearcher
    {
        IResults Search(string file, IQuery query);
    }

    public interface IQuery
    {
        string Query { get; }
        Language Language { get; }
        SearchOtions Option { get; }
        bool NewTranslateNeeded { get; }
    }

    public interface IAudioToText
    {
        bool AudioToTextConvert(string input, out string output, Language language);
    }

    public interface IVideoToAudio
    {
        bool VideoToAudioConvert(string input, out string output);
    }

    public enum Language
    {
        czech,
        english
    }

    public enum SearchOtions
    {
        Full_Match,
        Partial_Match,
        REGEXP_Match,
        Hamming_Match,
        Levenshtein_Match,
        DamerauLevenshtein_Match,
        WordToVector_Match
    }

    public interface IResult
    {
        string Context { get; set; }
        int Seconds { get; set; }

        string Match { get; set; }

    }
    public interface ISubtitle
    {
        int ID { get; set; }
        string Text { get; set; }
        string Match { get; set; }
        int BeginTimeSecond { get; set; }
        int EndTimeSecond { get; set; }
    }
    public interface IResults
    {
        string Source { get; set; }

        string AudioSource { get; set; }

        string SubtitleSource { get; set; }

        List<IResult> Found { get; set; }

        List<ISubtitle> Subtitles { get; set; }

        void AddResult(IResult result);

        bool failOnVideo { get; }
        bool failOnAudio { get; }
        bool failOnSrt { get; }

        bool isSuccessful { get; }

        string ErrorFileName { get; set; }

    }

    public interface ISearchInSubtitles
    {
        bool SearchInSubtitles(string input, IQuery query, IResults oldResults, out IResults results);
    }

    public interface ISearchInBlock
    {
        bool SearchInBlock(string input, IQuery query, out string match);
    }

}
