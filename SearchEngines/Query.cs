﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngines
{
    public class BasicQuery : IQuery
    {
        public  BasicQuery(string query,Language ChoosenLanguage,SearchOtions opt= SearchOtions.Full_Match ,bool newTranslate = false)
        {
            Query = query;
            Language = ChoosenLanguage;
            Option = opt;
            NewTranslateNeeded = newTranslate;
        }

        public string Query { get; }
        public Language Language { get; }
        public SearchOtions Option { get; }
        public bool NewTranslateNeeded{ get; }
}
}
