﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngines
{
    class Results : IResults
    {
        public string Source { get; set; }

        public string AudioSource { get; set; }

        public string SubtitleSource { get; set; }

        public List<IResult> Found { get ; set ; }

        public bool failOnVideo { get; set; }

        public bool failOnAudio { get; set; }

        public bool isSuccessful { get { return !failOnAudio && !failOnSrt && !failOnVideo; } }

        public bool failOnSrt { get; set; }
        public string ErrorFileName { get; set; }
        public List<ISubtitle> Subtitles { get; set; }

        public Results(string source)
        {
            Source = source;
            Found = new List<IResult>();
            Subtitles = new List<ISubtitle>();
            failOnVideo = false;
            failOnAudio = false;
            failOnSrt = false;
        }

        public Results()
        {}

        public void AddResult(IResult result)
        {
            Found.Add(result);
        }
    }

    class Result : IResult
    {
        public string Context { get; set; }

        public string Match { get; set; }
        public int Seconds { get; set; }

        public Result(int second, string context)
        {
            Seconds = second;
            Context = context;
        }

    }
}
