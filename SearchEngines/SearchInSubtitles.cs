﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngines
{
    class SearchInSubtitlesDefault : ISearchInSubtitles
    {
        public bool SearchInSubtitles(string input, IQuery query, IResults oldResults, out IResults results)
        {
            ISearchInBlock searcher = searchFactory(query);

            if (oldResults == null)
                results = new Results();
            else
                results = oldResults;

            if (!File.Exists(input)) return false;

            string[] lines = System.IO.File.ReadAllLines(input);

            int indexOfLine = 0;

            while (lines.Count() - 3 > indexOfLine)
            {
                oneScene actualScene = new oneScene();
                if (lines[indexOfLine] == "") indexOfLine++;  //Skip empty line
                
                int.TryParse(lines[indexOfLine++], out int id);
                actualScene.ID = id;

                var times = lines[indexOfLine++].Split(new string[] { " --> " }, StringSplitOptions.None);
                fixTimeToExpectedFormat(times);
                actualScene.BeginTimeSecond = (int)TimeSpan.Parse(times[0]).TotalSeconds;
                actualScene.EndTimeSecond = (int)TimeSpan.Parse(times[1]).TotalSeconds;
                actualScene.Text = lines[indexOfLine++];

                if (searcher.SearchInBlock(actualScene.Text, query, out string match))
                {
                    var res = new Result(actualScene.BeginTimeSecond, actualScene.Text);
                    res.Match = match;
                    actualScene.Match = match;
                    results.AddResult(res);
                }

                results.Subtitles.Add(actualScene);
            }

            return true;
        }

       

        /// <summary>
        ///
        /// </summary>
        /// <param name="time"></param>
        private void fixTimeToExpectedFormat(string[] time)
        {
            
            for (int i = 0; i < time.Length; i++)
            {
                time[i] = time[i].Replace(',', '.');
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private ISearchInBlock searchFactory(IQuery query)
        {
            switch (query.Option)
            {
                case SearchOtions.Full_Match: return new FullMatchSearchInBlock();

                case SearchOtions.Partial_Match: return new PartialFullMatchSearchInBlock();

                case SearchOtions.REGEXP_Match: return new RegexpSearchInBlock();

                case SearchOtions.DamerauLevenshtein_Match: return new ExperimentalMatch(ExperimentalMatch.OPTIONS.DamerauLevenshtein);

                case SearchOtions.Hamming_Match: return new ExperimentalMatch(ExperimentalMatch.OPTIONS.Hamming);

                case SearchOtions.Levenshtein_Match: return new ExperimentalMatch(ExperimentalMatch.OPTIONS.Levenshtein);

                case SearchOtions.WordToVector_Match: return new Word2VecSearchInBlock();

            }

            return new FullMatchSearchInBlock();
        }

    }

    struct oneScene : ISubtitle
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public string Match { get; set; }
        public int BeginTimeSecond { get; set; }
        public int EndTimeSecond { get; set; }
    }
}
