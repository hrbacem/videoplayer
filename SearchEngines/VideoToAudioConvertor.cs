﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngines
{
    public class  VideoToAudioConvertor : IVideoToAudio
    {
        public bool VideoToAudioConvert(string input, out string output)
        {
            string extension= Path.GetExtension(input); 
            if(".wav".Equals(extension))
            {
                output = input;
                return true;
            }
            output = input.Replace(extension, ".wav");
            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            try
            {
                ffMpeg.ConvertMedia(input, output, "wav");
            }
            catch(Exception e)
            {
                return false;
            }

            return true;
        }

    }

    
}
