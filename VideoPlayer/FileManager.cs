﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer
{
    static class FileManager
    {

        public static string[] getFiles()
        {
            var browser= new OpenFileDialog();
            browser.Multiselect = true;
            browser.ShowDialog();
            return browser.FileNames;
        }

    }
}
