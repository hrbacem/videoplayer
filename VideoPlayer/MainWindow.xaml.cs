﻿using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace VideoPlayer
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            MainSearchControl control = new MainSearchControl(this.QueryMaker);

            ResultsPanelsControl playlist = new ResultsPanelsControl(this.results,this.videoShow);
            ResultsStatusLog state = new ResultsStatusLog(this.fail, this.complete, this.all);
            ResultsConsoleLog logger = new ResultsConsoleLog();

            control.attach(playlist);
            control.attach(state);
            control.attach(logger);

            this.QueryMaker.runSearching += control.runSearch;    
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.results.Children.Clear();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SetModel mod = new SetModel();
            mod.Show();           
        }

        class MainSearchControl : Observable
        {
            List<Observer> observers = new List<Observer>();
            public void attach(Observer observer)
            {
                observers.Add(observer);
            }

            public void detach(Observer observer)
            {
                while(observers.Contains(observer))
                    observers.Remove(observer);
            }

            public void notify(IResults result)
            {
                foreach (var item in observers)
                {
                    item.update(result);
                }
            }

            public void anounceStart(int results)
            {
                foreach (var item in observers)
                {
                    item.startInformation(results);
                }
            }

            ISearcher engine = new DefaultSearchEngine();

            List<Task<IResults>> tasks = new List<Task<IResults>>();

            SearchControl queryBuilder;
 
            public MainSearchControl(SearchControl queryParams)
            {
                queryBuilder =  queryParams;
            }
            
            private IResults searching(string file, IQuery query)
            {
                IResults results = engine.Search(file, query);
                return results;
            }
            public void runSearch()
            {
                var originalCursor = Mouse.OverrideCursor;
                Mouse.OverrideCursor = Cursors.Wait;

                tasks.Clear();
                List<string> filesToSearch = this.queryBuilder.Files;
                IQuery query = this.queryBuilder.getBuildQuery();
                anounceStart(filesToSearch.Count);

                foreach (var item in filesToSearch)
                {
                    var t = Task.Run(() => searching(item, query));
                    tasks.Add(t);
                }

                try
                {
                    Task.WaitAny(tasks.ToArray());
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
                this.startMessaging();

                Mouse.OverrideCursor = originalCursor;

            }

            DispatcherTimer ShowTimer = new DispatcherTimer();            

            public void startMessaging()
            {
                ShowTimer.Interval = new TimeSpan(0, 0, 1);
                ShowTimer.Tick += updateSearching;
                ShowTimer.Start(); 
            }

            public List<IResults> preparedToSend = new List<IResults>();

            private void updateSearching(object sender, EventArgs e)
            {
                try
                {
                    List<Task<IResults>> taskDone = new List<Task<IResults>>();

                    foreach (var item in tasks)
                    {
                        if (item.IsCompleted)
                        {
                            notify(item.Result);
                            taskDone.Add(item);
                        }
                        else
                        {
                            if (item.IsFaulted)
                            {
                                notify(null);
                                taskDone.Add(item);
                            }

                        }

                    }
                    foreach (var item in taskDone)
                    {
                        if (tasks.Contains(item))
                            tasks.Remove(item);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                if (tasks.Count == 0)
                    ShowTimer.Stop();

            }
        }


        class ResultsStatusLog : Observer
        {
            int allFilesCount = 0;
            int completeFilesCount = 0;
            int failFilesCount = 0;

            Label failed;
            Label succed;
            Label searched;

            public ResultsStatusLog(Label fail, Label succ, Label search)
            {
                failed = fail;
                succed = succ;
                searched = search;
                allFilesCount = 0;
                completeFilesCount = 0;
                failFilesCount = 0;

                rewrite();
            }
            private void rewrite()
            {
                failed.Content = "Fail: " + failFilesCount;
                succed.Content = "Complete: " + completeFilesCount;
                searched.Content = "Search: " + (failFilesCount + completeFilesCount) + "/" + allFilesCount;
            }

            public void update(IResults results)
            {
                if (results == null)
                {
                    failFilesCount++;
                }
                else
                {
                    if (results.isSuccessful)
                        completeFilesCount++;

                    if (!results.isSuccessful)
                        failFilesCount++;
                }

                rewrite();
            }

            public void startInformation(int expectedResults)
            {
                allFilesCount = expectedResults;
                completeFilesCount = 0;
                failFilesCount = 0;
                rewrite();
            }
        }


        class ResultsConsoleLog : Observer
        {
            public void startInformation(int expectedResults)
            {
                Console.WriteLine("Expected results: "+expectedResults);
            }

            public void update(IResults update)
            {
                Console.WriteLine("Recieved: "+update.Source);
            }
        }

        class ResultsPanelsControl : Observer
        {
            StackPanel playableMenu;
            VideoControl videoPlayer;

            public ResultsPanelsControl(StackPanel targetMenu, VideoControl player)
            {
                playableMenu = targetMenu;
                videoPlayer = player;
            }

            public void startInformation(int expectedResults)
            {
                
            }

            public void update(IResults results)
            {
                if (results == null)
                {
                    MessageBox.Show("Total fail", "INVALID RESULT");
                    return;
                }

                if (results.failOnVideo)
                {
                    MessageBox.Show("Fail on video proccess " + results.ErrorFileName, "NO VIDEO");
                    return;
                }

                if (results.failOnAudio)
                {
                    MessageBox.Show("Fail on audio proccess " + results.ErrorFileName, "NO AUDIO");
                    return;
                }

                if (results.failOnSrt)
                {

                    MessageBox.Show("Fail on subtitle proccess " + results.ErrorFileName, "NO SUBTITLES");

                    return;
                }

                VideoPointEditor ve = new VideoPointEditor(results.Source);
                ve.Results = results.Found.Count.ToString();
                playableMenu.Children.Add(ve);
                foreach (var item in results.Found)
                {
                    ve.Add(new VideoPoint(videoPlayer, item.Seconds, results.Source, results.Subtitles, item.Context, item.Match));
                }
            }
        }
    }
}
