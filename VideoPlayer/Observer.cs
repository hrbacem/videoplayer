﻿using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer
{
    public interface Observer
    {
        void startInformation(int expectedResults);

        void update(IResults result);
    }

    public interface Observable
    {
        void attach(Observer observer);

        void detach(Observer observer);

        void notify(IResults result);

        void anounceStart(int result);
    }
}
