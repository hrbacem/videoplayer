﻿using Microsoft.Win32;
using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VideoPlayer
{
    /// <summary>
    /// Interaction logic for SearchControl.xaml
    /// </summary>
    public partial class SearchControl : UserControl
    {
        public SearchControl()
        {
            InitializeComponent();
            foreach (var item in Enum.GetValues(typeof(SearchOtions)).Cast<SearchOtions>())
            {
                options.Items.Add(item);
            }
            this.options.SelectedValue = SearchOtions.Full_Match;

            foreach (var item in Enum.GetValues(typeof(Language)).Cast<Language>())
            {
                this.languages.Items.Add(item);
            }
            this.languages.SelectedValue = SearchEngines.Language.czech;


            this.toScript.Items.Add(oldScriptUse);
            this.toScript.Items.Add("Make new script");
            this.toScript.SelectedValue = oldScriptUse;
        }
        string oldScriptUse = "Use old script";

        public delegate void SearchAction();
        public SearchAction runSearching;
        List<string> FilesToSearch = new List<string>();

        public IQuery getBuildQuery()
        {
            return new BasicQuery(TextToSearch, ChoosenLanguage, ChoosenOption, useNewScript());
        }
        public
        List<string> Files
        { get { return FilesToSearch; } }
        /*#################################################################################*/

        bool useNewScript()
        {
            return !(oldScriptUse == toScript.SelectedValue.ToString());
        }

        string TextToSearch { get { return this.searchText.Text.ToUpper(); } }

        Language ChoosenLanguage { get { return (Language)this.languages.SelectedValue; } }

        SearchOtions ChoosenOption { get { return (SearchOtions)this.options.SelectedValue; } }

        /*##############################################################*/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach (var file in FileManager.getFiles())
            {
                if (!FilesToSearch.Contains(file))
                    FilesToSearch.Add(file);
            }
            this.files.Items.Clear();
            foreach (var item in FilesToSearch)
            {
                this.files.Items.Add(item);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            runSearching?.Invoke();
        }

        private void Files_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            files.Items.Remove(files.SelectedItem);
        }

        private void Button_Click_Clear(object sender, RoutedEventArgs e)
        {
            this.files.Items.Clear();
            FilesToSearch.Clear();
        }
    }
}
