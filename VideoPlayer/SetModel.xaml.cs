﻿using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoPlayer
{
    /// <summary>
    /// Interaction logic for SetModel.xaml
    /// </summary>
    public partial class SetModel : Window
    {
        public SetModel()
        {
            InitializeComponent();
            cz = MODELS.CzechModel;
            en = MODELS.EnglishModel;
            this.maxDistance.Value = MODELS.MaximalDistance;
            czLabel.Content = "Czech model:\n" + cz;
            enLabel.Content = "English model:\n" + en;
        }

        string cz;
        string en;

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            cz = FileManager.getFiles().First();
            czLabel.Content = "Czech model:\n" + cz;
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            var fls = FileManager.getFiles();
            if (!fls.Any()) return;
            en = fls.First();
            enLabel.Content = "English model:\n" + en;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MODELS.CzechModel = cz;
            MODELS.EnglishModel = en;
            MODELS.MaximalDistance = maxDistance.Value;
            this.Close();
        }

        private void MinValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

        }

    }
}
