﻿using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace VideoPlayer
{
    /// <summary>
    /// Interaction logic for VideoControl.xaml
    /// </summary>
    public partial class VideoControl : UserControl
    {
        MediaElement mediaElement;
        DispatcherTimer timer;
        IList<ISubtitle> Subtitles;

        public VideoControl()
        {
            InitializeComponent();
            mediaElement = video;

            this.backgroundPalet.Visibility = Visibility.Visible;
            this.ManualPhoto.Visibility = Visibility.Visible;

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer.Tick += tick;
            timer.Start();

        }

        int TIME_TO_WAIT = 3000;
        int TIME_TO_SLEEP = 500;
        public void SetVideo(string videoUri, IList<ISubtitle> subtitles, int secondOffset = 0)
        {
            this.ManualPhoto.Visibility = Visibility.Hidden;
            this.backgroundPalet.Visibility = Visibility.Hidden;
            var originalCursor = Mouse.OverrideCursor;

            Mouse.OverrideCursor = Cursors.Wait;
            mediaElement.Stop();
            mediaElement.Source = new System.Uri(videoUri);
            mediaElement.Play();
            mediaElement.UnloadedBehavior = MediaState.Manual;

            if (subtitles == null)
                throw new Exception("EMPTY SUBTITLES");

            Subtitles = subtitles;
            jumpToVideo(secondOffset);

            if (mediaElement.NaturalDuration.HasTimeSpan)
                state.Maximum = (int)mediaElement.NaturalDuration.TimeSpan.TotalSeconds;

            state.Value = secondOffset;
            int counter = 0;

            while (!mediaElement.NaturalDuration.HasTimeSpan && TIME_TO_WAIT > counter)
            {
                Thread.Sleep(TIME_TO_SLEEP);
                counter += TIME_TO_SLEEP;
            }

            mediaElement.Source = new System.Uri(videoUri);
            jumpToVideo(secondOffset);
            if (mediaElement.NaturalDuration.HasTimeSpan)
                state.Maximum = (int)mediaElement.NaturalDuration.TimeSpan.TotalSeconds;

            state.Value = secondOffset;
            mediaElement.Play();
            ActualID = 0;
            actual = Subtitles[0];
            setSubtitles();
            Mouse.OverrideCursor = originalCursor;
        }


        /*#########################################################*/
        bool SET_BY_TIMER = false;

        private void jumpToVideo(int seconds)
        {
            video.Position = new TimeSpan(0, 0, 0, seconds, 0);
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Play();
        }

        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Pause();
        }

        bool IS_MOVING = false;
        bool WAS_MOVING = false;
        private void State_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!SET_BY_TIMER)
            {
                IS_MOVING = true;
                mediaElement.Pause();
            }
        }

        ISubtitle actual;
        int ActualID = 0;
        private void setSubtitles()
        {
            if (Subtitles == null) return;
            int time = (int)mediaElement.Position.TotalSeconds;
            while (ActualID + 1 < Subtitles.Count && Subtitles[ActualID + 1].BeginTimeSecond < time)
            {
                ActualID++;
            }
            actual = Subtitles[ActualID];

            setText(actual.Text, actual.Match);

        }

        private void setText(string text, string match)
        {
            if (match == null || match == "")
            {
                this.SubLabel.Text = text;
                return;
            }
            var inf = Regex.Split(text, match, RegexOptions.IgnoreCase);
            this.SubLabel.Text = "";
            this.SubLabel.TextWrapping = TextWrapping.Wrap;
            for (int i = 0; i < inf.Length - 1; i++)
            {
                this.SubLabel.Inlines.Add(inf[i]);
                this.SubLabel.Inlines.Add(new Run(match) { FontSize = this.SubLabel.FontSize + 2, Foreground = Brushes.Red });
            }
            this.SubLabel.Inlines.Add(inf.Last());
        }


        private void tick(object sender, EventArgs e)
        {
            if (IS_MOVING)
            {
                IS_MOVING = false;
                WAS_MOVING = true;
                return;
            }
            if (WAS_MOVING)
            {
                WAS_MOVING = false;
                video.Position = new TimeSpan(0, 0, 0, (int)state.Value, 0);
                mediaElement.Play();
                return;
            }
            SET_BY_TIMER = true;
            state.Value = mediaElement.Position.TotalSeconds;
            setSubtitles();
            SET_BY_TIMER = false;
        }
    }
}
