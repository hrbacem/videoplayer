﻿using SearchEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VideoPlayer
{
    /// <summary>
    /// Interaction logic for VideoPoint.xaml
    /// </summary>
    public partial class VideoPoint : UserControl
    {
      
        VideoControl videoControl;
        int second;
        string information;
        string videoUrl;
        IList<ISubtitle> Subtitles;

        public VideoPoint(VideoControl videoControl, int second, string videoUrl, IList<ISubtitle> subtitles, string information="NONE",string match ="")
        {
            InitializeComponent();
            this.videoControl = videoControl;
            this.information = information;
            this.second = second;
            this.videoUrl = videoUrl;
            Subtitles = subtitles;

            setText(information, match);
            
            TimeSpan t = TimeSpan.FromSeconds(second);
            this.momentTime.Text = 
                string.Format("{0:D2}h:{1:D2}m:{2:D2}s:{3:D3}ms",
                t.Hours,
                t.Minutes,
                t.Seconds,
                t.Milliseconds);
           
        }      

       private void setText(string text, string match)
        {
            var inf = Regex.Split(text, match, RegexOptions.IgnoreCase);
            this.context.Text = "";
            this.context.TextWrapping = TextWrapping.Wrap;
            for (int i = 0; i < inf.Length-1; i++)
            {
                this.context.Inlines.Add(inf[i]);
                this.context.Inlines.Add(new Run(match) { FontSize= this.context.FontSize+2, Foreground = Brushes.Red });
            }
            this.context.Inlines.Add(inf.Last());
        }

        private void Player_Click(object sender, RoutedEventArgs e)
        {
            videoControl.SetVideo(this.videoUrl,Subtitles, this.second);
        }
    }
}
