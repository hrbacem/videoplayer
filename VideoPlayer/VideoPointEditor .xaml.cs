﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VideoPlayer
{
    /// <summary>
    /// Interaction logic for VideoPointEditor.xaml
    /// </summary>
    public partial class VideoPointEditor : UserControl
    {
        public VideoPointEditor(string name)
        {
            InitializeComponent();
            this.videoName.Content = name;
            this.Height = MIN_height;
            IS_MAX = false;
        }

        public void Add(VideoPoint vp)
        {
            this.results.Children.Add(vp); 
        }

        public string Results { set => this.numRes.Content = "Results: " + value; }

        /*#########################################################*/
        const int MAX_height = 550;
        const int MIN_height = 210;
        bool IS_MAX = true;
        private void Size_Click(object sender, RoutedEventArgs e)
        {
            if (!IS_MAX)
            {
                this.Height = MAX_height;
                IS_MAX = true;
                this.size.Content = "-";
            }
            else
            {
                this.Height = MIN_height;
                IS_MAX = false;
                this.size.Content = "+";
            }
            
        }
    }
}
